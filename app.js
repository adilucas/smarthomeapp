const express = require('express')
const logger = require('morgan')
const path = require('path')
const mqtt = require('mqtt')
const colors = require('colors')
const devicesRouter = require('./routes')

const BROKER = 'mqtt://broker.hivemq.com'
const PORT = 3000

const devices = [{
    name: 'garage',
    state: '',
    connected: false,
},{
    name: 'rollerblinds',
    state: '',
    connected: false,
},{
    name: 'bedroomlights',
    state: '',
    connected: false,
}]

class Server {

    constructor() {
        this.app = express()
        this.mqttClient = mqtt.connect(BROKER)

        this.mqttClient.on('connect', () => {
            console.info(`Server:: Connected with MQTT broker ${BROKER.green} successfully`)
            devices.map(d => {
                console.info(`Subscribing ${d.name}/connected & ${d.name}/state`)
                this.mqttClient.subscribe(`${d.name}/connected`)
                this.mqttClient.subscribe(`${d.name}/state`)
            })
        })

        this.mqttClient.on('message', (topic, message) => {
            console.info(`Server:: Received message: ${topic}: ${message}`)
            devices.map(d => {
                switch (topic) {
                    case `${d.name}/connected`:
                        d.connected = this.handleConnected(d.name, message)
                        return d
                    case `${d.name}/state`:
                        d.state = this.handleState(d.name, message)
                        return d
                }
            })
        })

        this.config()
        this.app.listen(PORT, () => {
            console.info(`Server:: Server is listening on port ${String(PORT).yellow}`)
        })
    }

    handleConnected(device, message) {
        console.info(`${device} connected status ${message}`)
        return (message.toString() === 'True')
    }

    handleState(device, message) {
        console.info(`${device} state updated to ${message}`)
        return message
    }

    config() {
        this.app.use(logger('dev'))
        this.app.use(express.static('static'))
        // Przekazanie klienta mqtt, by był dostępny w obsłudze poszczególnych requestów
        this.app.use((req, res, next) => {
            req.mqttClient = this.mqttClient
            next()
        })
        this.setRoutes()
    }

    setRoutes() {
        // Set routes for API
        this.app.use('/devices', devicesRouter)
        // Render home page
        this.app.get('/', (req, res) => {
            res.sendFile(path.join(__dirname, 'index.html'))
        })
        // Default route
        this.app.all('*', (req, res) => {
            res.status(404).json({ message: 'No device matched' })
        })
    }
}

const app = new Server()
