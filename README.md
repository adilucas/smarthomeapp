# SmartHomeApp #
> Lightweight beta application with IoT mechanisms taking advantage of MQTT protocol

## Dependencies:
* node.js@8.x.x
* python@3.6.x

## How to run
1. Install modules required for app with ```npm install```
2. Open a terminal in root directory of the project and run ```node app```
3. You should see that server is running on port 3000 (*http://localhost:3000*) and connection to MQTT server has been established
4. In another terminal, go to (```cd```) root directory and run ```py listener.py garage``` (You can also use rollerblinds or bedroomlights instead of garage)
5. You should now see that server has discovered that device has connected to MQTT broker
6. Test connection by typing in another terminal ```curl localhost:3000/devices/garage/open```, or just enter that URL to your web browser

## API routes

* **device** - device matcher, could be any of ```['garage', 'rollerblinds', 'bedroomligths']```
* **action** - what action should be taken with given device (open or close)

Then the URL for API is being built like below:
```http://localhost:3000/<device>/<action>```

Note: All parameters are required and every device has 2 types of action (```['open', 'close']```)