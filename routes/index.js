const express = require('express')
const router = express.Router()

router.get('/:device/:action', (request, response) => {
    const device = request.params.device
    const action = request.params.action

    if (['garage', 'rollerblinds', 'bedroomlights'].includes(device) && ['open', 'close'].includes(action)) {
        request.mqttClient.publish(`${device}/${action}`, 'True')
        return response.json({ device, action, message: 'Published'})
    }

    return response.status(400).json({ device, action, message: 'Unrecognized device or/and action' })
})

module.exports = router
