#!/usr/bin/env python


'''
IoT device mock

to run:
- sudo chmod +x listener.py
- ./listener.py {garage, rollerblinds, bedroomlights}

to exit:
- Ctrl + C
'''


import paho.mqtt.client as mqtt
from sys import argv


TOPIC = str(argv[1])


def is_connected():
    print('--->[' + TOPIC.upper() + ']: DEVICE CONNECTED')


def power_on(client):
    client.publish(TOPIC + '/state', 'opened')
    print('--->[' + TOPIC.upper() + ']: OPENED')


def power_off(client):
    client.publish(TOPIC + '/state', 'closed')
    print('--->[' + TOPIC.upper() + ']: CLOSED')


def opened():
    print('--->[' + TOPIC.upper() + ']: "OPENED" message has been published')


def closed():
    print('--->[' + TOPIC.upper() + ']: "CLOSED" message has been published')


def on_connect(client, userdata, rc):
    print('Connected with result code ' + str(rc))
    client.subscribe([
        (TOPIC + '/connected', 0),
        (TOPIC + '/open', 0),
        (TOPIC + '/close', 0),
        (TOPIC + '/state', 0)		# FOR TESTS
    ])
    client.publish(TOPIC + '/connected', 'True')
    client.publish(TOPIC + '/open', 'True')         # FOR TESTS
    client.publish(TOPIC + '/close', 'True')  # FOR TESTS


def on_message(client, userdata, msg):
    topic = str(msg.topic)
    payload = str(msg.payload.decode('UTF-8'))
    if topic == TOPIC + '/connected' and payload == 'True':
        is_connected()
    elif topic == TOPIC + '/open' and payload == 'True':
        power_on(client)
    elif topic == TOPIC + '/close' and payload == 'True':
        power_off(client)
    elif topic == TOPIC + '/state' and payload == 'opened':
        opened()
    elif topic == TOPIC + '/state' and payload == 'closed':
        closed()
    else:
        raise RuntimeError('Unexpected message has been received: ['
                           + str(topic) + ']: ' + str(payload))


def main():
    if not len(argv) == 2:
        raise RuntimeError('Only one argument can be passed.')
    elif not (TOPIC == 'garage' or TOPIC == 'rollerblinds'
              or TOPIC == 'bedroomlights'):
        raise ValueError('Unexpected argument: "' + argv[1]
                         + '" Please pass one of these: {garage, rollerblinds, '
                         + 'bedroomlights}')
    else:
        client = mqtt.Client()
        client.on_connect = on_connect
        client.on_message = on_message
        client.connect('broker.hivemq.com', 1883, 60)
        client.loop_forever()
        return 0


if __name__ == '__main__':
    main()
